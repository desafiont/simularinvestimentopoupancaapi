package apiSimulador;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsInRelativeOrder;
import static org.hamcrest.Matchers.is;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.junit.Test;

public class AcessoAPI {

	@Test

	public void acessarAPI() {

		RestAssured.baseURI = "http://5b847b30db24a100142dce1b.mockapi.io/";
		RequestSpecification request = RestAssured.given().relaxedHTTPSValidation();
		Response response = request.when().get("api/v1/simulador").prettyPeek();
		Assert.assertTrue(response.statusCode() == 200);

		}
}