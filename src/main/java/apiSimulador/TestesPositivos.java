package apiSimulador;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

public class TestesPositivos {

    @Test

    public void acessarId() {

        RestAssured.baseURI = "http://5b847b30db24a100142dce1b.mockapi.io/";
        RequestSpecification request = RestAssured.given().relaxedHTTPSValidation();
        Response response = request.when().get("api/v1/simulador").prettyPeek();
        Assert.assertTrue(response.statusCode() == 200);
        Assert.assertEquals(response.jsonPath().get("id"), 1);

    }

    @Test
    public void acessarMeses() {

        RestAssured.baseURI = "http://5b847b30db24a100142dce1b.mockapi.io/";
        RequestSpecification request = RestAssured.given().relaxedHTTPSValidation();
        Response response = request.when().get("api/v1/simulador").prettyPeek();

        Assert.assertTrue(response.statusCode() == 200);
        Assert.assertEquals(response.jsonPath().getList("meses").size(),4);
        Assert.assertEquals(response.jsonPath().get("meses[0]"), "112");

        Integer intnum = Integer.parseInt((String) response.jsonPath().get("meses[3]"));
        Assert.assertTrue(intnum>100);

         }
    @Test
    public void acessarValor() {

        RestAssured.baseURI = "http://5b847b30db24a100142dce1b.mockapi.io/";
        RequestSpecification request = RestAssured.given().relaxedHTTPSValidation();
        Response response = request.when().get("api/v1/simulador").prettyPeek();

        Assert.assertTrue(response.statusCode() == 200);
        Assert.assertEquals(response.jsonPath().getList("valor").size(),4);
        Assert.assertEquals(response.jsonPath().get("valor[0]"), "2.802");

        Float numf1 = Float.parseFloat((String) response.jsonPath().get("valor[2]"));
        Assert.assertTrue(numf1>3.500f);

        Float numf2 = Float.parseFloat((String) response.jsonPath().get("valor[3]"));
        Assert.assertTrue(numf2<4.000f);

    }

   }
